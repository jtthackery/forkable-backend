const express = require('express');
const axios = require('axios');

const app = express();

const port = 3000;

const baseURL = 'https://maps.googleapis.com/maps/api/place/details/json';

const key = '';

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/placeDetails/:id', (req, res) => {
    console.log(`req received: ${req.params.id}`);
    axios
        .get(`${baseURL}?placeid=${req.params.id}&key=${key}`)
        .then((response) => {
            res.send(response.data);
        })
        .catch((error) => {
            console.log(error);
        });
});

app.listen(port, () => console.log(`listening on port ${port}`));